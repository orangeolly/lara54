<?php

namespace Tests\Unit;

use App\Post;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $firstPost = factory(Post::class)->create();
        $secondPost = factory(Post::class)->create([
            'created_at'=> Carbon::now()->subDays(32)
        ]);
        $posts = Post::archive();
        self::assertCount(2,$posts);
    }
}
