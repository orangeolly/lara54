@extends('blog.layouts.master')


@section('content')
    <div class="col-md-8 blog-main">
        <h1>Register a user</h1>

        <hr>

        <form method="post" action="/register">
            {{csrf_field()}}
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control"  name="name" required>
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" id="email" name="email" class="form-control" required ></input>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" id="email" name="password" class="form-control" required ></input>
            </div>
            <div class="form-group">
                <label for="password_confirmation">Password Copnfirmation</label>
                <input type="password" id="email" name="password_confirmation" class="form-control" required ></input>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Register</button>
            </div>
        </form>

        @include('layouts.errors')
    </div>
@endsection