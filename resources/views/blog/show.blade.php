@extends('blog.layouts.master')


@section('content')
    <div class="col-md-8 blog-main">
        <h1>{{$post->title}}</h1>
        {{$post->body}}

        <br>
        By {{$post->user->name}}
        <hr>

        @if(count($post->tags))
            <ul>
                @foreach($post->tags as $tag)
                    <li>{{$tag->name}}</li>
                @endforeach
            </ul>
            <hr>
        @endif

        <div class="comments">
            @foreach($post->comments as $comment)

            <article>

                {{$comment->body}}
                <br>
                {{$comment->created_at->diffForHumans()}}
                <br>
                From:{{$comment->user->name}}&nbsp{{$comment->user->email}}

            </article>

                <hr>

            @endforeach

        </div>

        <div class="card">
            <div class="card-block">

                <form method="POST" action='/posts/{{ $post->id }}/comments'>
                    {{csrf_field()}}
                    <div class="form-group">

                        <textarea name="body" placeholder="Your comment here." class="form-control" required></textarea>

                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Submit your comment
                        </button>
                    </div>
                </form>
                @include('layouts.errors')


            </div>
        </div>


    </div>
@endsection