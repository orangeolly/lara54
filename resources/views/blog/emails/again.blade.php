@component('mail::message')
# Hello here is a line with a hash.

{{$name}} is new
========
This i some body text

* Olly
* Julie
* Dom
* Jonny

---


@component('mail::button', ['url' => 'https://google.com'])
    Google it!
@endcomponent
@component('mail::panel', ['url' => 'https://google.com'])
    Lorem ipsum dolar sit amet
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
