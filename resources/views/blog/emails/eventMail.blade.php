@component('mail::message')
# Introduction

Hello {{$user->name}} the event just fired

@component('mail::button', ['url' => ''])
Do not push this button
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
