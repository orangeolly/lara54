<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>

        <title>Task list</title>

    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    Your tasks are
                </div>
                <ul>
                @foreach($tasks as $task)
                    <li>
                        <a href="/tasks/{{$task->id}}"> {{$task->body}}</a>
                    </li>
                @endforeach
                </ul>
            </div>
        </div>
    </body>
</html>
