@extends('blog.layouts.master')


@section('content')
    <div class="col-md-8 blog-main">
        <h1>Login</h1>

        <hr>

        <form method="post" action="/login">
            {{csrf_field()}}
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" id="email" name="email" class="form-control" required ></input>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password"  name="password" class="form-control" required ></input>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Login</button>
            </div>
        </form>

        @include('layouts.errors')
    </div>
@endsection