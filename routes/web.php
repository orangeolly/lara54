<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Task;

//$stripe = resolve('App\Billing\Stripe');
//dd($stripe);

Route::get('/','PostsController@index')->name('home');

Route::get('/posts/create','PostsController@create');

Route::post('/posts','PostsController@store');

Route::get('/posts/{post}','PostsController@show');

Route::post('/posts/{post}/comments','CommentsController@store');

Route::get('posts/tags/{tag}','TagsController@index');

Route::get('/register','RegistrationController@create');
Route::post('/register','RegistrationController@store');
Route::get('/login','SessionController@create')->name('login');
Route::post('/login','SessionController@store');
Route::get('/logout','SessionController@destroy');
//Route::post('/posts/{post}/comments',function(){
//    dd('in here');
//});

//Route::get('/blog',function(){
//    return view('blog.index');
//});
//Route::get('/posts/{post}',function(){
//    dd("in posts function for show");
//});

//
//Route::get('about',function(){
//    return view('about');
//});
//
//Route::get('tasks/{task}',function($id){
////    $task = DB::table('tasks')->find($id);
//    $task = Task::find($id);
////    dd($task);
//    return view('tasks.show',compact('task'));
//});
//
//Route::get('tasks/',function(){
////    $tasks = DB::table('tasks')->get();
//    $tasks = Task::all();
////    dd($task);
//    return view('tasks.index',compact('tasks'));
//});

//Route::resources([
//  //  'tasks'=>'TaskController',
//    'posts'=>'PostsController',
//]);