<?php

namespace App\Providers;

use App\Billing\Stripe;
use App\Post;
use App\Tag;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        DB::listen(function ($query) {
            Log::info($query->sql);
            Log::info($query->bindings);
            // $query->time
        });

        view()->composer('blog.sidebar', function($view){
            $view->with('archives', Post::archive())
                ->with('tags', Tag::has('posts')->pluck('name'));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /// used to register classes into the service container.
        ///
        $this->app->bind(Stripe::class,function(){
            return new Stripe(config('services.stripe.secret'));
        });
    }
}
