<?php
/**
 * Created by PhpStorm.
 * User: Olly
 * Date: 3/22/2018
 * Time: 10:56 AM
 */

namespace App;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Model extends Eloquent
{
    protected $guarded = [];
}