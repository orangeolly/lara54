<?php

namespace App;



class Comment extends Model
{

    public function __construct(array $attributes = array())
    {
        $this->user_id = User::first()->id;
        parent::__construct($attributes);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
