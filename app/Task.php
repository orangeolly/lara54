<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    public function is_complete()
    {
        return false;
    }

    public function scopeIncomplete($query)
    {
        return $query->where('complete',0);
    }    //
}
