<?php

namespace App;

use Carbon\Carbon;

class Post extends Model
{

    public function __construct(array $attributes = array())
    {
        $this->user_id = auth()->id();
        parent::__construct($attributes);
    }
//
//
    public function comments()
    {
//        return $this->hasMany('App\Comment'); /// could also have used Comment::class here which just returns the same string.
//        return $this->hasMany('Comment'); /// could also have used Comment::class here which just returns the same string.
        return $this->hasMany(Comment::class); /// could also have used Comment::class here which just returns the same string.
        ///
    }

    public function createComment($body,$user_id): void
    {
        $this->comments()->create(compact('body','user_id'));
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeFilter($query,$filters)
    {
       //dd($filters);
        if ($month = $filters['month'])
        {
            $query->whereMonth('created_at', Carbon::parse($month)->month);
        }
        if ($year = $filters['year'])
        {
            $query->whereYear('created_at', $year);
        }
    }

    /**
     * @return mixed
     */
    public static function archive()
    {
        $archives = static::selectRaw('year(created_at) year,monthname(created_at) month, count(*) published')
            ->groupBy('year', 'month')
            ->orderByRaw('min(created_at) desc')
            ->get();
        return $archives;
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
