<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;

class SessionController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest',['except'=>'destroy']);
    }
    public function create()
    {
        return view('sessions.create');
    }

    public function store()
    {
        //$user = User::first();

        if (! auth()->attempt(request(['email','password'])))
        {
            return back()->withErrors(
                [
                    'message'=>'Please check your password and try again'
                ]
            );
        }
        ///auth()->login($user);
        /// this is done automatically by attempt above.

        return redirect()->home();
    }

    public function destroy()
    {
        auth()->logout();
        return redirect()->home();
    }
}
