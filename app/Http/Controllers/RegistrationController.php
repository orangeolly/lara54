<?php

namespace App\Http\Controllers;

use App\Mail\WelcomeAgain;
use App\Mail\WelcomeEmail;
use App\User;
use function bcrypt;
use Illuminate\Http\Request;
use Mail;

class RegistrationController extends Controller
{
    public function create()
    {
        return view('registration.create');
    }

    public function store(Request $request)
    {
//        dd('here');

        $this->validate($request,[

            'name'=>'required',
            'email'=>'required|email',
            'password'=>'required|confirmed',

        ]);
        $request['password']=bcrypt($request['password']);
        $user = User::create($request->input());

        /// should log the user in.
        auth()->login($user);
        Mail::to($user)->send(new WelcomeAgain($user));
        session()->flash('message', 'Thanks for registering with New oily vision');
        return redirect()->home();
    }

}


